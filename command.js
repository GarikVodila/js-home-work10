let tab = function () {
    let tabNav = document.querySelectorAll(".tabs-title"),
      tabContent = document.querySelectorAll(".tab"),
      tabName;
  
    tabNav.forEach((item) => {
      item.addEventListener("click", selectTabNav);
    });
  
    function selectTabNav() {
      tabNav.forEach((item) => {
        item.classList.remove("active");
      });
      this.classList.add("active");
      tabName = this.getAttribute("data-tab-name");
      selectTabContent(tabName);
    }
  
    function selectTabContent(tabName) {
      tabContent.forEach((item) => {
        if (item.getAttribute("data-tab-name") === tabName) {
          item.classList.add("active");
        } else {
          item.classList.remove("active");
        }
      });
    }
  };
  
  tab();
  